import { CARD_CLICK, RESTART_GAME, CHANGE_THEME } from '../Constants/memory';
import createCardsData from "../Data/CardsData"
import { flipCard, flipDownCards } from "../Helpres/cardsHelper"
const initialTheme = "food";
const initialState = {
    cards : createCardsData(initialTheme),
    theme: initialTheme,
    clickedCards : [],
    score: 0,
    startTime: new Date()
};

export default function memory(state = initialState, action) {
    switch (action.type) {

        case CARD_CLICK:
            const clickedCard = action.card;
            let newScore = state.score;
            let newCardsData = state.cards;

            if (clickedCard.flipped) {
                //Pull the card out of the clicked cards
                newCardsData = flipCard(newCardsData, clickedCard);
                state.clickedCards = state.clickedCards.filter(card => {
                    return card.id !== clickedCard.id
                });
                return {...state, 
                    cards: newCardsData,
                }
            } 
            if (state.clickedCards.length === 1) {
                state.clickedCards.push(clickedCard);
                newCardsData = flipCard(newCardsData, clickedCard);
                if(state.clickedCards[0].imgUrl === clickedCard.imgUrl && 
                    state.clickedCards[0].id !== clickedCard.id) {
                    // They match! :)
                    state.clickedCards = [];
                    newScore++;
                }
                return {...state, 
                    cards: newCardsData,
                    score: newScore
                }
            }
            if (state.clickedCards.length === 2){ 
                    newCardsData = flipDownCards(newCardsData, state.clickedCards[0], state.clickedCards[1])
                    state.clickedCards = [];
                    state.clickedCards.push(clickedCard);
                    newCardsData = flipCard(newCardsData, clickedCard);
                    return {...state, 
                        cards: newCardsData,
                    }
            } else {
                state.clickedCards.push(clickedCard);
                newCardsData = flipCard(newCardsData, clickedCard);
                return {...state, 
                    cards: newCardsData,
                }
            }

        break;
        case RESTART_GAME :

            return {...state,
                cards : createCardsData(initialTheme),
                clickedCards : [],
                score: 0,
                startTime: new Date()
            };
            

        case CHANGE_THEME :
                const newTheme = state.theme === "app" ? "food" : "app";
                return {...state, 
                    theme: newTheme,
                    cards : createCardsData(newTheme),
                    clickedCards : [],
                    score: 0,
                }

        default:
            return state;
    }
}