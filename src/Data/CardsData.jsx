export default (theme) => {
    let cards = [];
    generateCards(cards, theme)
    shuffle(cards)
    return cards;
}
 
function generateCards(cards, theme) {
    for (let i = 0; i < 16; i+=2) {

        cards.push({
            id : i,
            flipped : false,
            imgUrl : `${theme}${i}`,
        });
        cards.push({
            id: i +1,
            flipped : false,
            imgUrl : `${theme}${i}`,
        });
    }
    return cards;
}

function shuffle(array) {
    let currentIndex = array.length, temp, randomIndex ;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temp = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temp;
    }

    return array;
}