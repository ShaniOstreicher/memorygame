const images = {
    app0: require('./app0.jpg'),
    app2: require('./app2.jpg'),
    app4: require('./app4.jpg'),
    app6: require('./app6.jpg'),
    app8: require('./app8.jpg'),
    app10: require('./app10.jpg'),
    app12: require('./app12.jpg'),
    app14: require('./app14.jpg'),
    food0: require('./food0.png'),
    food2: require('./food2.jpg'),
    food4: require('./food4.png'),
    food6: require('./food6.jfif'),
    food8: require('./food8.png'),
    food10: require('./food10.png'),
    food12: require('./food12.png'),
    food14: require('./food14.png'),
    back: require('./back.jfif'),
};

export default images;