import React  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Header from "./Header"
import CardsTable from "./CardsTable"
import Info from "./Info"

const useStyles = makeStyles(theme => ({
  header: {
    padding: "3vh"
  },
}));

const Card = (props) => {

  const classes = useStyles();
  return (
    <Grid container >
        <Grid item xs={12} className={classes.header}>
            <Header></Header>
        </Grid>
        <Grid item xs={12} >
            <CardsTable></CardsTable>
        </Grid>
        <Grid item xs={12} >
            <Info ></Info>
        </Grid>
    </Grid>
  );
};

export default Card;
