import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  header: {
    fontFamily: "Comic Sans MS"
  }
}));

const Header = (props) => {

  const classes = useStyles();
  return (
    <Grid item xs={12} className={classes.header}>
      <div>Memory Game Online
Train your brain! 
Find pairs of images under the following tiles:</div>
    </Grid>
  );
};

export default Header;
