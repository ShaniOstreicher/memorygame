import React, { useState, useEffect }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from "react-redux";
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';

import memory from "../Actions/memory"

const useStyles = makeStyles(theme => ({
  header: {
    fontFamily: "Comic Sans MS"
  }
}));

const Info = (props) => {

 const dispatch = useDispatch();
 
 const theme =  useSelector(
   (state) => state.memory.theme
 );

 const [checkedTheme, setCheckedTheme] = useState(theme);

 const handleClick = () => {
    dispatch(memory.restart())
  };

  const handleThemeChange = (event) => {
    setCheckedTheme(event.target.checked);
    dispatch(memory.changeTheme())
  };

  const score =  useSelector(
    (state) => state.memory.score
  );
  

  const startTime =  useSelector(
    (state) => state.memory.startTime
  );

  function millisToMinutesAndSeconds(millis) {
    millis = millis - startTime;
    var minutes =  Math.floor((millis / (1000 * 60)) % 60);
    var seconds =  Math.floor((millis / 1000) % 60);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  }
  
  const [timer, setTimer] = useState(new Date());


  useEffect(() => {
        setInterval(() => {
          setTimer(new Date());
        }, 1000)
    }, [timer]);


  const classes = useStyles();
  return (
    <Grid container diraction="column" spacing={1} >
        <Grid item xs={12} className={classes.header}>
        <Button variant="contained" color="secondary" onClick={handleClick} >restart game</Button>
        </Grid>
        <Grid item xs={12} className={classes.header}>
            <div>{millisToMinutesAndSeconds(timer)}</div>
        </Grid>
        <Grid item xs={12} className={classes.header}>
            {score < 8 ? 
                (<div> your score: {score}</div>): 
                (<div> you won the game!</div>)
            }
        </Grid>
        <Grid item xs={12} className={classes.header}>
          apps
        <Switch
          checked={checkedTheme}
          onChange={handleThemeChange}
          color="primary"
          name="checkedTheme"
          />
          food
        </Grid>
    </Grid>
  );
};

export default Info;
