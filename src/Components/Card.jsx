import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from "react-redux";
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Images  from '../Data/index.js';
import memory from "../Actions/memory"

const useStyles = makeStyles(theme => ({
  card: {
    padding: "3vh"
  },
  img: {
    width: "6vh",
    height: "6vh"
  }
}));

const Card = (props) => {
  const dispatch = useDispatch();
  const {card} = props;

const handleClick = () => {
  dispatch(memory.cardClick(card))
};

  const classes = useStyles();
  const img = card.flipped ? Images[card.imgUrl] : Images.back
  return (
    <Grid className={classes.card}>
      <Avatar variant="square" src={img} className={classes.img} onClick={handleClick}></Avatar>
    </Grid>
  );
};

export default Card;
