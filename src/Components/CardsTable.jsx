import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from "react-redux";
import Grid from '@material-ui/core/Grid';
import Card from './Card';

const useStyles = makeStyles(theme => ({
    cardTable: {
      backgroundColor: "#8C9A9E",
      "text-align": "-webkit-center",
      padding: "4vh"  
    },
  }));

  const CardsTable = () => {
  
  const renderCards = (cards) => {
    return (cards.map((card, index) => {
        return (
          <Grid item xs={3}>
            <Card card={card} key={index} ></Card>
          </Grid>
      )
      })
    )
  };
  
    const cards =  useSelector(
      (state) => state.memory.cards
    );
    const classes = useStyles();
    return (
    <Grid container diraction="column" spacing={3} className={classes.cardTable}>
        <Grid container diraction="row" item xs={12}>
        {renderCards(cards.slice(0, 4))}
        </Grid>
        <Grid container diraction="row" item xs={12}>
        {renderCards(cards.slice(4, 8))}
        </Grid>
        <Grid container diraction="row" item xs={12}>
        {renderCards(cards.slice(8, 12))}
        </Grid>
        <Grid container diraction="row" item xs={12}>
        {renderCards(cards.slice(12, 16))}
        </Grid>
        </Grid>
)
};

export default CardsTable;