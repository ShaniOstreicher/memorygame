import { CARD_CLICK, RESTART_GAME, CHANGE_THEME } from '../Constants/memory';

export default {

    cardClick : (card) => ({
        type : CARD_CLICK,
        card
    }),

    restart : () => ({
        type : RESTART_GAME
    }),

    changeTheme: theme => ({
        type: CHANGE_THEME,
        theme
    })

}