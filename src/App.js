import React from 'react';
import './App.css';
import { Provider } from 'react-redux';

import {store} from './Store/store';
import MemoryGamePage from "./Components/MemoryGamePage"

function App() {
  return (
    <Provider store={store}>
    <div className="App">
     <MemoryGamePage></MemoryGamePage>
    </div>
    </Provider>
  );
}

export default App;
