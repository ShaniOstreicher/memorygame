export function flipCard(cards, flippedCard) {
    return cards.map(card => {
        return card.id === flippedCard.id ? 
            Object.assign({}, card, { flipped : !flippedCard.flipped }) : 
            Object.assign({}, card)
    })
}

export function flipDownCards(cards, flippedCard1, flippedCard2) {
    return cards.map(card => {
        return card.id === flippedCard1.id || card.id === flippedCard2.id ? 
            Object.assign({}, card, { flipped : false }) : 
            Object.assign({}, card)
    })
}
